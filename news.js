const apiKey = "08fb2e71e7624577bc3907cace5f98f6";
const searchInput = document.getElementById("searchInput");
const newsContainer = document.getElementById("newsContainer");

const fetchNews = async (searchKeyword) => {
  const url = `https://newsapi.org/v2/everything?q=${searchKeyword}&apiKey=${apiKey}`;

  try {
    const response = await axios.get(url);
    return response.data.articles;
  } catch (error) {
    console.error("Error fetching news:", error);
    return [];
  }
};

const renderNews = (articles) => {
  newsContainer.innerHTML = "";

  if (articles.length === 0) {
    newsContainer.innerHTML = "<p>Tidak ada berita yang ditemukan.</p>";
    return;
  }

  articles.forEach((article) => {
    const newsCard = `
      <div class="col-md-4 mb-4">
        <div class="card">
          <img src="${
            article.urlToImage || "https://via.placeholder.com/150"
          }" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${article.title}</h5>
            <p class="card-text">${article.description}</p>
            <a href="${
              article.url
            }" target="_blank" class="btn btn-primary">Baca Selengkapnya</a>
          </div>
        </div>
      </div>
    `;

    newsContainer.insertAdjacentHTML("beforeend", newsCard);
  });
};

const showLoadingIndicator = () => {
  newsContainer.innerHTML =
    "<div class='text-center'><div class='spinner-border' role='status'><span class='sr-only'>Loading...</span></div></div>";
};

const defaultSearchKeyword = "trending";
const handlePageLoad = async () => {
  showLoadingIndicator();
  const articles = await fetchNews(defaultSearchKeyword);
  renderNews(articles);
};

const handleSearch = async () => {
  const searchKeyword = searchInput.value.trim();
  if (searchKeyword === "") {
    return;
  }
  showLoadingIndicator();
  const articles = await fetchNews(searchKeyword);
  renderNews(articles);
};

searchInput.addEventListener("input", handleSearch);
handlePageLoad();
